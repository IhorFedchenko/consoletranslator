package beta;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
//


/**
 * Created by qwert on 27.05.2016.
 */
public class Menu {
    Dictionary dictionary = new Dictionary();

    public static void main(String[] args) throws IOException {

        Menu menu = new Menu();
        menu.runMenu();
    }

    private void runMenu() throws IOException {
        int choice = -1;
        printHeader();
        while (choice != 0) {
            printMenu();
            choice = getInput();
            performAction(choice);
        }
    }

    private void printHeader() {
        System.out.println("+---------------------------------------+");
        System.out.println("|     Welcome to console translator     |");
        System.out.println("+---------------------------------------+");
    }

    private void printMenu() {
        System.out.println("\nPlease make a selection:");
        System.out.println("1) Translate en-ru");
        System.out.println("2) Add word");
        System.out.println("3) Remove word");
        System.out.println("4) Edit word");
        System.out.println("5) Print all dictionary");
        System.out.println("0) Exit");
    }

    private int getInput() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int choice = -1;
        while (choice < 0 || choice > 5) {
            try {
                System.out.print("\nEnter your choice: ");
                choice = Integer.parseInt(reader.readLine());
            } catch (NumberFormatException e) {
                System.out.println("Invalid selection. Please try again.");
            }
        }
        return choice;
    }

    public void performAction(int choice) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        switch (choice) {
            case 1:
                System.out.print("Enter word for translate: ");
                dictionary.getTranslate(reader.readLine());
                break;
            case 2:
                System.out.println("Add the word to the dictionary");
                System.out.println("Enter english word");
                String engAdd = reader.readLine();
                System.out.println("Enter translate");
                String ruAdd = reader.readLine();
                dictionary.testAdd(engAdd, ruAdd);
                break;
            case 3:
                System.out.println("Enter the word removed from the dictionary");
                dictionary.removeWord(reader.readLine());

                break;
            case 4:
                System.out.println("Enter eng word to edit");
                String engEdit = reader.readLine();
                System.out.println("Enter rus word to edit");
                String rusEdit = reader.readLine();
                dictionary.editDict(engEdit, rusEdit);
                break;
            case 5:
                System.out.println("Print all dictionary");
                dictionary.printDict();
                break;
        }
    }
}
