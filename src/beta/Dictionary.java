package beta;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by qwert on 27.05.2016.
 */
public class Dictionary {
    private HashMap<String, String> dict = new HashMap<String, String>();

    Dictionary() {
        dict.put("break", "ломать");
        dict.put("can", "мочь");
        dict.put("come", "приходить");
        dict.put("cook", "готовить");
        dict.put("fall", "падать");
        dict.put("have", "иметь");
        dict.put("tell", "сказать");
        dict.put("wait", "ждать");
        dict.put("start", "начинать");
        dict.put("stop", "останавливать");
    }

    protected void getTranslate(String inputString) {
        System.out.println(dict.get(inputString) == null ? "Слово отсутствует в словаре" : dict.get(inputString));
    }

    protected void addWord(String engWord, String ruWord) {
        if (dict.containsKey(engWord)) {
            System.out.println("Внимание! " + engWord + " слово уже есть в словаре");
        } else {
            dict.put(engWord, ruWord);
            System.out.println("Слово и перевод успешно добавленны в словарь");
        }
    }

    protected void removeWord(String word) {
        System.out.println(dict.remove(word) == null ? "Слово отсутствует в словаре" : "Слово удаленно");
    }

    protected void editDict(String engWord, String ruWord) {

        dict.put(engWord, ruWord);
    }

    protected void printDict() {

        for (Map.Entry<String, String> pair : dict.entrySet()) {
            String key = pair.getKey();
            String value = pair.getValue();
            System.out.println(key + " : " + value);
        }
    }

    protected boolean containsKey(String inputString) {

        return dict.containsKey(inputString);
    }

    protected void testAdd(String a, String b){
        System.out.println(dict.put(a, b));
    }

}
